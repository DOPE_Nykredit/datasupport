package com.nykredit.kundeservice.table;

import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;

public class right_click_menu extends JPopupMenu {
	private static final long serialVersionUID = 1L;
	JMenuItem MarkAllMenuItem;
	JMenuItem copyMenuItem;
	BankTable potLiveTable;

	public right_click_menu(BankTable potLiveTable){
		add(getMarkAllMenuItem());
		add(getCopyMenuItem());
		this.potLiveTable = potLiveTable;
		}
	
		private JMenuItem getMarkAllMenuItem() {
			if (MarkAllMenuItem == null) {
				MarkAllMenuItem = new JMenuItem();
				MarkAllMenuItem.setText("Mark�r alt");
				MarkAllMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
						Event.CTRL_MASK, true));
				MarkAllMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						potLiveTable.selectAll();
					}
				});
			}
			return MarkAllMenuItem;
		}

		private JMenuItem getCopyMenuItem() {
			if (copyMenuItem == null) {
				copyMenuItem = new JMenuItem();
				copyMenuItem.setText("Kopi�r");
				copyMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
						Event.CTRL_MASK, true));
				copyMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						javax.swing.Action copy = potLiveTable.getActionMap().get("copy");
						ActionEvent ae = new ActionEvent(potLiveTable, ActionEvent.ACTION_PERFORMED, "");
						copy.actionPerformed(ae); 
					}
				});
			}
			return copyMenuItem;
		}
	} 