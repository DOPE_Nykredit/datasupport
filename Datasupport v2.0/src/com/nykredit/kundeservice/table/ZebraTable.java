package com.nykredit.kundeservice.table;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ZebraTable extends DefaultTableCellRenderer
{
  private static final long serialVersionUID = 1L;

  public Component getTableCellRendererComponent(JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int column)
  {
    Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);

    if (row % 2 == 0) {
      cell.setBackground(new Color(200, 210, 255));
      cell.setForeground(Color.black);
    } else {
      cell.setBackground(new Color(255, 255, 200));
      cell.setForeground(Color.black);
    }

    return cell;
  }
}