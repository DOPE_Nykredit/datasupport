package com.nykredit.kundeservice.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.SwingConstants;

public class RestTableRender extends ZebraTable {
	private static final long serialVersionUID = 1L;

	public Component getTableCellRendererComponent (JTable table,Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
		Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);
      
		if (column == 0){this.setHorizontalAlignment(SwingConstants.LEFT);} 
		else {setHorizontalAlignment(SwingConstants.CENTER);}

		if(row == 0) {
            cell.setBackground(Color.red);
		} else if("".contentEquals((String)table.getValueAt(row, 5))) {
            cell.setBackground(Color.gray);
            cell.setFont(new Font("sansserif", Font.BOLD+Font.ITALIC, 14));
		} else if("Total".contentEquals((String)table.getValueAt(row, 0))) {
			cell.setBackground(Color.green);
        } else if (column == 0) {
			cell.setBackground(cell.getBackground().darker());
        }/* else if("0".contentEquals((String)table.getValueAt(row, column))) {
            cell.setForeground(cell.getBackground());
		}*/
    	
    	return cell;
	}
}