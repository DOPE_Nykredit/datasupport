package com.nykredit.kundeservice.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;

public class TableRender extends ZebraTable {
	private static final long serialVersionUID = 1L;

	public Component getTableCellRendererComponent (JTable table,Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
		Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);
      
		setHorizontalAlignment(SwingConstants.CENTER);

		if(" ".contentEquals((String)table.getValueAt(row, 1)) ||
			"".contentEquals((String)table.getValueAt(row, 1))) {
			cell.setBackground(new Color(212,148,48));
		}

    	return cell;
	}
}