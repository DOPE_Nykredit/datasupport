package com.nykredit.kundeservice.table;

import com.nykredit.kundeservice.util.Formatter;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class BankTable extends JTable implements MouseListener {
	private static final long serialVersionUID = 1L;
	
	private DefaultTableModel thisModel;
	Formatter dope = new Formatter();

/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Initialize XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	public BankTable(){
		this.getTableHeader().setPreferredSize(new Dimension(this.getTableHeader().getWidth(),40));
		addMouseListener(this);
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private void FileUnrap(BufferedReader bufRdr) throws IOException {
		String line = null;
		int row = 0;
		while((line = bufRdr.readLine()) != null) {	
			StringTokenizer st = new StringTokenizer(dope.erstat(line,"\"",""),";");
			if (row == 0) {
				if (FileHeader(st)) break;
			} else
				FileData(st);
			row++;
		} 
		bufRdr.close();
	}
	private boolean FileHeader(StringTokenizer st) {
		thisModel= new DefaultTableModel();
		getTableHeader().setReorderingAllowed(false);
		String test = "", single = "";
		while (st.hasMoreTokens()) {
			single = st.nextToken();
			test += single;
			thisModel.addColumn ((Object) single);
		}

		setColumnSelectionAllowed(false);
		setRowSelectionAllowed(false);
		setCellSelectionEnabled(true);
		
		setDefaultRenderer(Object.class, new TableRender());
		
		setModel (thisModel);
		this.setAutoResizeMode(5);
		return ! test.equalsIgnoreCase("EKSPNRNykredit_Inittypeantal");
	}
	private void FileData(StringTokenizer st) {
		Object d[] = new Object[20];
		int col = 0;
		while (st.hasMoreTokens())	{
			d[col] = dope.cutFromFirst(st.nextToken(),",");
			col++;
		}
		thisModel.addRow(d);
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */	
	public void FileRead(String filsti) {
		
		try {
			File file = new File(filsti);
			FileUnrap(new BufferedReader(new FileReader(file)));
		}
		catch (FileNotFoundException e) {}
		catch (IOException e) {}
	}
	public void Empty() {
		thisModel= new DefaultTableModel();
		setModel (thisModel);
	}
	@Override
	public void mousePressed(MouseEvent e){
		if (e.isPopupTrigger())
			doPop(e);
		}
	@Override
	public void mouseReleased(MouseEvent e){
		if (e.isPopupTrigger())
			doPop(e);
		}
	private void doPop(MouseEvent e){
		right_click_menu menu = new right_click_menu(this);
		menu.show(e.getComponent(), e.getX(), e.getY());
	}
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}

}
