package com.nykredit.kundeservice.system;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.TooManyListenersException;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;

public class FileDrop {
	public static interface Listener {
		public abstract void filesDropped( File[] files );
    }   // end inner-interface Listener
	
    private transient Border normalBorder;
    private transient DropTargetListener dropListener;
    
/** Discover if the running JVM is modern enough to have drag and drop.
 */	private static Boolean supportsDnD;
    
    // Default border color
    private static Color defaultBorderColor = new Color( 0f, 0f, 1f, 0.25f );
    private Border bdr = BorderFactory.createMatteBorder( 2, 2, 2, 2, defaultBorderColor);
    
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Class Constructor XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */

    /**
     * Constructs a {@link FileDrop} with a default light-blue border
     * and, if <var>c</var> is a {@link java.awt.Container}, recursively
     * sets all elements contained within as drop targets, though only
     * the top level container will change borders.
     *
     * @param c Component on which files will be dropped.
     * @param listener Listens for <tt>filesDropped</tt>.
     * @since 1.0
     */
    public FileDrop(final Component c,final Listener listener) {   
    	final PrintStream out = null;
        if( supportsDnD() ) {   // Make a drop listener
            dropListener = new DropTargetListener() {   public void dragEnter( DropTargetDragEvent evt ) {	

                    // Is this an acceptable drag event?
                    if( isDragOk(evt ) ) {
                        // If it's a Swing component, set its border
                        if( c instanceof JComponent )
                        {   JComponent jc = (JComponent) c;
                            normalBorder = jc.getBorder();
                            jc.setBorder( bdr );
                        }

                        // Acknowledge that it's okay to enter
                        //evt.acceptDrag( java.awt.dnd.DnDConstants.ACTION_COPY_OR_MOVE );
                        evt.acceptDrag( DnDConstants.ACTION_COPY );
                    }
                    else {   // Reject the drag event
                        evt.rejectDrag();
                    }
                }

                public void dragOver( DropTargetDragEvent evt ) {   // This is called continually as long as the mouse is
                    // over the drag target.
                }

                @SuppressWarnings({ "unchecked", "rawtypes" })
				public void drop( DropTargetDropEvent evt )  {   
                    try {   // Get whatever was dropped
                        Transferable tr = evt.getTransferable();

                        // Is it a file list?
                        if (tr.isDataFlavorSupported (DataFlavor.javaFileListFlavor)) {
                            // Say we'll take it.
                            //evt.acceptDrop ( DnDConstants.ACTION_COPY_OR_MOVE );
                            evt.acceptDrop ( DnDConstants.ACTION_COPY );

                            // Get a useful list
                            List fileList = (List) 
                                tr.getTransferData(DataFlavor.javaFileListFlavor);
                            // Convert list to array
                            File[] filesTemp = new File[ fileList.size() ];
                            fileList.toArray( filesTemp );
                            final File[] files = filesTemp;

                            // Alert listener to drop.
                            if( listener != null )
                                listener.filesDropped( files );

                            // Mark that drop is completed.
                            evt.getDropTargetContext().dropComplete(true);
                        } else {
                            DataFlavor[] flavors = tr.getTransferDataFlavors();
                            boolean handled = false;
                            for (int zz = 0; zz < flavors.length; zz++) {
                                if (flavors[zz].isRepresentationClassReader()) {
                                    // Say we'll take it.
                                    //evt.acceptDrop ( DnDConstants.ACTION_COPY_OR_MOVE );
                                    evt.acceptDrop(DnDConstants.ACTION_COPY);

                                    Reader reader = flavors[zz].getReaderForText(tr);

                                    BufferedReader br = new BufferedReader(reader);
                                    
                                    if(listener != null)
                                        listener.filesDropped(createFileArray(br));
                                    
                                    // Mark that drop is completed.
                                    evt.getDropTargetContext().dropComplete(true);
                                    handled = true;
                                    break;
                                }
                            }
                            if(!handled) {
                                evt.rejectDrop();
                            }
                        }
                    }
                    catch ( IOException io) {   
                        io.printStackTrace( out );
                        evt.rejectDrop();
                    }
                    catch (UnsupportedFlavorException ufe) {   
                        ufe.printStackTrace( out );
                        evt.rejectDrop();
                    }
                    finally {
                        // If it's a Swing component, reset its border
                        if( c instanceof JComponent ) {
                        	JComponent jc = (JComponent) c;
                            jc.setBorder( normalBorder );
                        }
                    }
                }

                public void dragExit( DropTargetEvent evt ) {   
                    // If it's a Swing component, reset its border
                    if( c instanceof JComponent ) {
                    	JComponent jc = (JComponent) c;
                        jc.setBorder( normalBorder );
                    }
                }

                public void dropActionChanged( DropTargetDragEvent evt ) {   
                    // Is this an acceptable drag event?
                    if( isDragOk(evt) ) {
                    	//evt.acceptDrag( DnDConstants.ACTION_COPY_OR_MOVE );
                        evt.acceptDrag( DnDConstants.ACTION_COPY );
                    } else {
                    	evt.rejectDrag();
                    }
                }
            };

            // Make the component (and possibly children) drop targets
            makeDropTarget(c, true);
        }
    }

/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */ 
	private static boolean supportsDnD() {
        if( supportsDnD == null ) {   
            boolean support = false;
            try {
				support = true;
            } catch( Exception e ) {
            	support = false;
            	System.out.println("Do not support DnDConstants !\n" + e);
            }
            supportsDnD = new Boolean( support );
        }
        return supportsDnD.booleanValue();
    }
    
     private static String ZERO_CHAR_STRING = "" + (char)0;
     
     @SuppressWarnings({ "rawtypes", "unchecked" })
     private static File[] createFileArray(BufferedReader bReader) {
    	 try {
    		 List list = new ArrayList();
    		 String line = null;
    		 while ((line = bReader.readLine()) != null) {
    			 try {
    				 // kde seems to append a 0 char to the end of the reader
    				 if(ZERO_CHAR_STRING.equals(line)) continue;

    				 File file = new File(new URI(line));
    				 list.add(file);
    			 } catch (Exception ex) {
    				 
    			 }
    		}

            return (File[]) list.toArray(new File[list.size()]);
        } catch (IOException ex) {
        }
        return new File[0];
     }
    
    private void makeDropTarget(final Component c, boolean recursive )
    {
        // Make drop target
        final DropTarget dt = new DropTarget();
        try {
        	dt.addDropTargetListener( dropListener );
        }
        catch( TooManyListenersException e )
        {   e.printStackTrace();
        }
        
        // Listen for hierarchy changes and remove the drop target when the parent gets cleared out.
        c.addHierarchyListener( new HierarchyListener() {
        	public void hierarchyChanged( HierarchyEvent evt ) {
                Component parent = c.getParent();
                if( parent == null ) {
                	c.setDropTarget( null );
                }
                else {
                	new DropTarget(c, dropListener);
                }
            }
        });
        if( c.getParent() != null )
            new DropTarget(c, dropListener);
        
        if( recursive && (c instanceof Container ) ) {   
            // Get the container
            Container cont = (Container) c;
            
            // Get it's components
            Component[] comps = cont.getComponents();
            
            // Set it's components as listeners also
            for( int i = 0; i < comps.length; i++ )
                makeDropTarget(comps[i], recursive );
        }
    }
    
    
    
    /** Determine if the dragged data is a file list. */
    private boolean isDragOk(final DropTargetDragEvent evt ) {
    	boolean ok = false;
        
        // Get data flavors being dragged
        DataFlavor[] flavors = evt.getCurrentDataFlavors();
        
        // See if any of the flavors are a file list
        int i = 0;
        while( !ok && i < flavors.length )
        {   
            // Is the flavor a file list?
            final DataFlavor curFlavor = flavors[i];
            if( curFlavor.equals( DataFlavor.javaFileListFlavor ) ||
                curFlavor.isRepresentationClassReader()){
                ok = true;
            }
            i++;
        }
        return ok;
    }
    
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */	
} 