package com.nykredit.kundeservice.system;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.table.TableModel;

import com.nykredit.kundeservice.data.CTIRConnection;

public class cBank extends CTIRConnection {
	
	private ResultSet rs = null;
	private String dato = " DATO ER IKKE ANGIVET !! ",
					pan = "";

/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Oracle Statements XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private void MaxDato() {
		setQ(
		"SELECT MAX(\"Dato\") FROM KS_DRIFT.POST_FAX");
	}
	private void Insert(String eksp, String init, String type, String anta) {
		String string = "('" + init + "'" +
						",'" + eksp + "'" +
						",'" + type + "'" +
						",'" + anta + "'" +
						",'" + dato + "')";
		try {
			Hent_intet(
			"INSERT INTO KS_DRIFT.POST_FAX " +
			"(\"Initialer\", \"Eksp NR\", \"Type\", \"Antal\", \"Dato\") " +
			"VALUES " + string);
		} catch (SQLException e) {
			System.err.println("Can't insert " + string + "\nException: " + e);
		}
	}
	private void Delete() {
		try {
			Hent_intet(
			"DELETE FROM KS_DRIFT.POST_FAX 	" +
			"WHERE \"Dato\" >= (			" +
			"	SELECT MAX(\"Dato\")		" +
			"	FROM KS_DRIFT.POST_FAX		" +
			") 								");
		} catch (SQLException e) {
			System.err.println("Exception: " + e);
		}
	}
	private void Korrigeret(String Agents) {
		setQ(
			"SELECT AA.INITIALER											" +
			"FROM KS_DRIFT.V_TEAM_DATO AA									" +
			"LEFT JOIN KS_DRIFT.PERO_AGENT_CBR BB							" +
			"ON AA.DATO = BB.\"Dato\" 										" +
			"AND AA.INITIALER = BB.\"Initialer\"							" +
			"WHERE AA.DATO = '" + dato + "'									" +
			"AND (BB.\"Korrigeret\" < 3/24 OR BB.\"Korrigeret\" IS NULL)	" +
			"AND AA.INITIALER IN (" + Agents + ")							");
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private void setQ(String query) {
		try {rs = Hent_tabel(query);}
		catch (SQLException e) {System.out.println("Exception: "  + e);}
	}
	private String getAgents(TableModel table) {
		String result = "";
		for ( int row = 0 ; row < table.getRowCount() ; row++ ) {
			if ( row > 0 ) result += ",";
			result += "'" + table.getValueAt(row, 1) + "'";
		}
		return result;
	}
	private boolean allAtWork() {
		pan = "\n";
		try {
			int count = 0 ;
			while ( rs.next() ) {
				count++ ;
				if (count > 1) pan += ", ";
				if (count % 6 == 0) pan += "\n  "; 
				pan += rs.getString(1);
			}
			return ( count < 4 );
		} catch (SQLException e) {
			System.err.println("Exception: " + e);
			return true;
		}
	}
	public boolean PromtOverf�r(boolean JustGo) {
		if (JustGo) return true;
		Object[] options = {"Ja", "Nej"};
		int confirmed = JOptionPane.showOptionDialog(null,
			"Agenter p� listen,der ikke har log-in:" + pan +
			"\n\nVil du overf�rer alligevel?",
			">> Mismatch <<",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,     	//do not use a custom Icon
			options,  	//the titles of buttons
			options[0] 	//default button title
		);
		
		return (confirmed == 0);
	}
	private void toOracle(TableModel table) {
		for ( int row = 0 ; row < table.getRowCount() ; row++ ) {
			Insert( (String)table.getValueAt(row, 0),
					(String)table.getValueAt(row, 1),
					(String)table.getValueAt(row, 2),
					(String)table.getValueAt(row, 3));
		}
	}
	public boolean PromtDelete() {
		Object[] options = {"Ja", "Nej"};
		int confirmed = JOptionPane.showOptionDialog(null,
			"Er du sikker p� at du vil slette Oracle?",
			">> STOP <<",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,     	//do not use a custom Icon
			options,  	//the titles of buttons
			options[0] 	//default button title
		);
		
		return (confirmed == 0);
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */	
	public String getMaxDato() {
		MaxDato();
		try {
		    rs.next();
		    return rs.getString(1);
		} catch (SQLException e) {
			System.out.println("Exception: "+e);
			return "";
		}
	}
	public void setDate(String dato) {
		this.dato = dato.substring(0, 10);
	}
	public void overf�r(TableModel table) {
		String Agents = getAgents(table);
		Korrigeret(Agents);
		if (PromtOverf�r(allAtWork())) {
			toOracle(table);
		}
	}
	public boolean slet() {
		if (PromtDelete()) {
			Delete();
			return true;
		}
		return false;
	}
}