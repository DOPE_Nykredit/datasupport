package com.nykredit.kundeservice.system;

import com.nykredit.kundeservice.swing.Calendar;
import com.nykredit.kundeservice.util.Date;
import com.nykredit.kundeservice.util.SF;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;


public class DateChoose extends JFrame {
	private static final long serialVersionUID = 1L;

	private SF sf = new SF() {public void func() {}};
	private JLabel DateLabel = null;
	private JLabel DaysLabel = null;
	private JLabel KontLabel = null;
	private Date Date = new Date();
	private Date Mini = new Date();
	boolean ma = true,
			ti = true,
			on = true,
			to = true,
			fr = true,
			l� = false,
			s� = false;

	public JScrollPane make(int left, int top, String Date) {
		this.Date.override(Date.substring(0, 10) + " 00:00:00:000");
		this.Mini.override(this.Date.ToString());
		this.Date.edit('D', 1);
		JScrollPane scrollPaneDatePicker = new JScrollPane();
		scrollPaneDatePicker.setBounds(new Rectangle(left, top, 210, 39));
		scrollPaneDatePicker.setViewportView(getJDesktopPane());
		return scrollPaneDatePicker;
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private void change() {
		Calendar lol = new Calendar(Date.ToString(),ma,ti,on,to,fr,l�,s�);
		Date.override(lol.getDate());
		lol = null;
	}
	private void update() {
		DateLabel.setText(Date.ToString("dd-MM-yyyy"));
		DaysLabel.setText(Date.getName('D'));
		if (getKontrol()) {
			KontLabel.setText("Dato OK");
			KontLabel.setBackground(new Color(146,208,80));
		} else {
			KontLabel.setText("Fejl 40");
			KontLabel.setBackground(new Color(192,0,0));
		}
		sf.func();
	}
	private JDesktopPane getJDesktopPane() {
		JDesktopPane mainPane = new JDesktopPane();
		mainPane.setOpaque(false);
		DateLabel = label(-1, 18, Date.ToString("dd-MM-yyyy"));
		DateLabel.setBackground(new Color(255,255,0));
		DaysLabel = label(69, 18, Date.getName('D'));
		KontLabel = label(139, 18, "Fejl 40");
		mainPane.add(label(-1, -1, "Dato"), null);
		mainPane.add(label(69, -1, "Dag"), null);
		mainPane.add(label(139, -1, "Kontrol"), null);
		mainPane.add(DateLabel, null);
		mainPane.add(DaysLabel, null);
		mainPane.add(KontLabel, null);
		mainPane.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				change();
				update();
			}
		});
		update();
		return mainPane;
	}
	private JLabel label(int x, int y, String text) {
		JLabel lab = new JLabel(text,SwingConstants.CENTER);
		lab.setBorder(BorderFactory.createLineBorder(Color.gray));
		lab.setBounds(new Rectangle(x, y, 71, 20));
		lab.setOpaque(true);
		lab.setBackground(Color.white);
		return lab;
	}
	private void setFirst() {
		this.Date.override(this.Mini.ToString());
		boolean stop = false;
		while (! stop) {
			this.Date.edit('D', 1);
			switch (Date.getInfo('B')) {
			case 1:	stop = ma;	break;
			case 2:	stop = ti;	break;
			case 3:	stop = on;	break;
			case 4:	stop = to;	break;
			case 5:	stop = fr;	break;
			case 6:	stop = l�;	break;
			case 7:	stop = s�;	break;
			}
			update();
		}
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */	
	public String getDato() {
		return Date.ToString("yyyy-MM-dd");
	}
	public String getMinimum() {
		return Mini.getName('D') +" den "+ Mini.getInfo('D') +". "+ Mini.getName('M') +" "+ Mini.getInfo('Y');
	}
	public boolean getKontrol() {
		return Date.isGreaterThan(Mini.ToString());
	}
	public void setMinimum(String date) {
		this.Mini.override(date.substring(0, 10) + " 00:00:00:000");
		setFirst();
	}
	public void setDays(boolean ma, boolean ti, boolean on, boolean to, boolean fr, boolean l�, boolean s�) {
		this.ma = ma;
		this.ti = ti;
		this.on = on;
		this.to = to;
		this.fr = fr;
		this.l� = l�;
		this.s� = s�;
		setFirst();
	}
	public void setEvent(final SF sf) {
		this.sf = sf;
	}
}