package com.nykredit.kundeservice.main;

import com.nykredit.kundeservice.swing.NFrame;
import com.nykredit.kundeservice.swing.PredefinedSwingComponents;
import com.nykredit.kundeservice.system.DateChoose;
import com.nykredit.kundeservice.system.FileDrop;
import com.nykredit.kundeservice.system.cBank;
import com.nykredit.kundeservice.table.BankTable;
import com.nykredit.kundeservice.util.SF;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PrinterException;
import java.io.File;
import java.sql.SQLException;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
//a
public class Bank extends NFrame
{
  private static final long serialVersionUID = 1L;
  private BankTable Table = null;
  private String program = null;
  private cBank oracle = new cBank();
  private JScrollPane SPBankTable = null;
  private PredefinedSwingComponents dope = new PredefinedSwingComponents();
  private DateChoose date = new DateChoose();
  JTextArea WF = null;
//Test
  public Bank(String prog)
  {
    this.program = prog;
    try {
      this.oracle.Connect(); } catch (SQLException e) {
      System.err.println("Exception: " + e);
    }this.oracle.SetupSpy(this.program);

    setSize(672, 397);
    setContentPane(Pane());
    setTitle(this.program);
    setResizable(false);
    setLocationRelativeTo(getRootPane());
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
        Bank.this.dope.PromtClosing(Bank.this.program, Bank.this);
      }
    });
    setVisible(true);
  }
//asass
  private JPanel Pane()
  {
    JPanel Pane = new JPanel();
    Pane.setLayout(new BorderLayout());
    Pane.add(DPane(), "Center");
    return Pane;
  }
  private JDesktopPane DPane() {
    JDesktopPane mainPane = new JDesktopPane();
    mainPane.setOpaque(false);
    mainPane.add(this.date.make(10, 197, this.oracle.getMaxDato()), null);
    mainPane.add(Workflow(10, 24, this.date.getMinimum()), null);
    mainPane.add(label(12, 7, "1. Hent mail"), null);
    mainPane.add(label(12, 175, "2. Kontroll�r data"), null);
    mainPane.add(this.dope.CopyRight("Drift og Performance", 1400, 345), null);
    mainPane.add(this.dope.Button("Administrativ - slet sidste", 10, 245, new SF() { public void func() { Bank.this.Admin();
      }
    }), null);
    mainPane.add(this.dope.Button("Overf�r til Oracle", 10, 270, new SF() { public void func() { Bank.this.Overf�r();
      }
    }), null);
    mainPane.add(this.dope.Button("Udskriv", 10, 295, new SF() { public void func() { Bank.this.udskriv();
      }
    }), null);
    mainPane.add(this.dope.Button("Luk program", 10, 320, new SF() { public void func() { Bank.this.luk();
      }
    }), null);
    mainPane.add(getBankTable(), null);
    this.date.setDays(true, true, true, true, true, false, false);
    return mainPane;
  }
  private JScrollPane getBankTable() {
    this.Table = new BankTable();
    this.SPBankTable = new JScrollPane();
    this.SPBankTable.setBounds(new Rectangle(228, 10, 430, 335));
    this.SPBankTable.getViewport().setBackground(Color.white);
    this.SPBankTable.setViewportView(this.Table);
    new FileDrop(this.SPBankTable, new FileDrop.Listener() {
      public void filesDropped(File[] files) {
        Bank.this.Table.FileRead(files[0].getPath());
      }
    });
    return this.SPBankTable;
  }
  private void Admin() {
    if (this.oracle.slet())
      refresh(); 
  }

  private void Overf�r() {
    if (this.date.getKontrol()) {
      this.oracle.setDate(this.date.getDato());

      this.oracle.overf�r(this.Table.getModel());
      refresh();
    }
  }

  private void udskriv() {
    try {
      this.Table.print(); } catch (PrinterException e) {
      e.printStackTrace();
    }
  }
  private void luk() {
    this.dope.PromtClosing(this.program, this);
  }
  private JLabel label(int x, int y, String text) {
    JLabel lab = new JLabel();
    lab.setText(text);
    lab.setFont(new Font("Vedana", 1, 12));
    lab.setBounds(new Rectangle(x, y, 210, 20));
    lab.setOpaque(false);
    return lab;
  }
  private JTextArea Workflow(int x, int y, String MaxDag) {
    if (this.WF == null) {
      this.WF = new JTextArea();
      this.WF.setOpaque(false);
      this.WF.setFont(new Font("Vedana", 0, 9));
      this.WF.setBounds(new Rectangle(x, y, 210, 160));
    }
    this.WF.setText(
      "1.1 �ben Outlook\n1.2 Find mail fra Adm Optimering fra dags dato \n1.3 Tr�k den vedh�ftede excel fil til tabel-\n      omr�det til h�jre\nMailen vil typisk indeholde data fra forrige\n�bningsdag, hvis mandag hentes for fredag, l�rdag og s�ndag\nDato for dataen skal v�re nyere end hvad der i\nforvejen findes i Oracle. Oracle indeholder data\nfra " + 
      MaxDag);
    return this.WF;
  }
  private void refresh() {
    this.Table.Empty();
    this.date.setMinimum(this.oracle.getMaxDato());
    Workflow(10, 24, this.date.getMinimum());
  }
}
