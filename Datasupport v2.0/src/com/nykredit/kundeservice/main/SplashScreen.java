package com.nykredit.kundeservice.main;

import com.nykredit.kundeservice.swing.NDialog;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.UIManager;

public class SplashScreen extends NDialog
{
  private static final long serialVersionUID = 1L;
  public static String program = "DataSupport Bank v2.0";
  private JLabel labelSplash = null;

  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(
        UIManager.getSystemLookAndFeelClassName());
    } catch (Exception localException) {
    }
    SplashScreen splash = new SplashScreen();
    Bank bank = new Bank(program);
    bank.setDefaultCloseOperation(0);
    splash.dispose();
  }
//hello world
  public SplashScreen() {
    setSize(400, 150);
    setLocationRelativeTo(getRootPane());
    setUndecorated(true);
    setContentPane(getLabelSplash());
    setTitle(program);
    setVisible(true);
  }

  private JLabel getLabelSplash() {
    if (this.labelSplash == null) {
      this.labelSplash = new JLabel(program, 0);
      this.labelSplash.setOpaque(true);
      this.labelSplash.setBackground(new Color(34, 56, 127));
      this.labelSplash.setForeground(Color.white);
      this.labelSplash.setFont(new Font("Verdana", 2, 25));
    }
    return this.labelSplash;
  }
}